import React, {useState} from 'react';
import Entrance from "./components/Entrance";
import MainContent from "./components/MainContent";


function App() {
  const [isEntered, setIsEntered] = useState<boolean>(false)

  return (
    <div>
      <Entrance onEnter={() => setIsEntered(true)}/>
        {
            isEntered && <MainContent />
        }
    </div>
  );
}

export default App;
