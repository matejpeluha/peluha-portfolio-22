
export interface TimelineItem {
    position: string,
    company?: string
    date: string,
    description?: string,
    image: string
    url?: string
}

export interface TimeLineContent {
    studies?: TimelineItem[],
    jobs?: TimelineItem[]
    tech?: TimelineItem[],
    travels?: TimelineItem[]
}

export interface ContentMenuItem {
    key: string,
    title: string
}

export interface SkillsContent {
    name: string
    level: number
    activeYears?: number
}

export interface Contact {
    mail?: string,
    phone?: string,
    linkedin?: string
}

export class Texts {
    static fullName: string = 'Petra Peluhová'
    static entranceButton: string = 'Kto som?'
    static birthYear: string = '1975'
    static country: string = 'Slovensko'
    static slogan: string = "'Verím v ľudskosť a tvrdú prácu'"
    static cvDownload: string = "Životopis"
    static cvDownloadUrl: string = '/pdfs/petra_peluhova_cv.pdf'

    static contact: Contact = {
        mail: "petapeluhova@gmail.com",
        phone: "+421 902 909 006"
    }

    static menu: ContentMenuItem[] = [
        {
            key: "studies",
            title: "Štúdium"
        },
        {
            key: "jobs",
            title: "Práca"
        },
        {
            key: "skills",
            title: "Zručnosti"
        },
        {
            key: "contact",
            title: "Kontakt"
        }
    ]

    static timeLineContent: TimeLineContent = {
        studies: [
            {
                position: "Kurz jednoduchého účtovníctva",
                date: "2006",
                description: "Spoločnosť REVA 3426/2006",
                image: '/images/reva.png'
            },
            {
                position: "Kurz podvojného účtovníctva",
                date: "2004",
                description: "Spoločnosť REVA 1174/2004",
                image: '/images/reva.png'
            },
            {
                position: "Maturita",
                company: "Stredná priemyselná škola, Kvačalova ul.",
                date: "1989 - 1993",
                description: "Prevádzkyschopnosť výrobných zariadení",
                image: '/images/kvacalova.png'
            },
        ],
        jobs: [
            {
                position: "Administratívno-technický pracovník",
                company: "ESP spol. s r.o",
                date: "09/2018 - doteraz",
                image: '/images/esp.png'
            },
            {
                position: "Odborný pracovník-realizácia aktivít\n" +
                    "Národného projektu NPC II-BA kraj-Rastový\n" +
                    "program",
                company: "Slovak Business Agency",
                date: "04/2018 - 06/2018",
                image: '/images/sba.png'
            },
            {
                position: "Finančná manažérka NP CSD II.",
                company: "Implementačná agentúra Ministerstva práce,\n" +
                    "sociálnych vecí a rodiny SR",
                date: "02/2017 - 02/2018",
                image: '/images/iampvsr.jpeg'
            },
            {
                position: "Asistentka finančného manažéra NP RaP a\n" +
                    "asistentka projektového manažéra NP POS",
                company: "Implementačná agentúra Ministerstva práce,\n" +
                    "sociálnych vecí a rodiny",
                date: "08/2015 - 01/2017",
                image: '/images/iampvsr.jpeg'
            },
            {
                position: "Referent administrácie ŽoP národného\n" +
                    "projektu",
                company: "Slovenská agentúra pre cestovný ruch",
                date: "08/2013 - 07/2015",
                image: '/images/sacr.jpeg'
            },
            {
                position: "Účtnovníctvo (SZČO)",
                date: "1989 - 07/2015",
                image: '/images/contract.webp'
            }
        ]
    }


    /* my personal skills */
    // static timeLineContent: TimeLineContent = {
    //     studies: [
    //         {
    //             position: "Master degree",
    //             company: "STU FEI",
    //             date: "09/2021 - until now",
    //             description: "My master thesis is web app implementing LR(k) analyser for non-context-free grammars. I used React, Node, Nest and Typescript.",
    //             image: '/images/stufei.png'
    //         },
    //         {
    //             position: "Bachelor thesis award",
    //             date: "STU FEI, 2021",
    //             description: "My bachelor thesis obtained award from Dean. It was used as base for a lot of other bachelor theses and base for complex university tool.",
    //             image: '/images/fei_award.png',
    //             url: "https://gitlab.com/matejpeluha/ptester"
    //         },
    //         {
    //             position: "Bachelor degree",
    //             company: "STU FEI",
    //             date: "09/2018 - 07/2021",
    //             description: "My bachelor thesis is desktop app with usage of multithreading for unit testing of student homework programms in C and C++. I was honoredI used C++ with Qt.",
    //             image: '/images/stufei.png'
    //         },
    //     ],
    //     jobs: [
    //         {
    //             position: "Full Stack Developer",
    //             company: "AMCEF",
    //             date: "09/2022 - until now",
    //             description: "I am working on multiple projects as Medior Web Full Stack Developer. Most of the time I am using technologies like React, Node, Next and Typescript.",
    //             image: '/images/amcef.png'
    //         },
    //         {
    //             position: "Full Stack Developer",
    //             company: "UXtweak",
    //             date: "09/2021 - 09/2022",
    //             description: "I was developing a tool used for testing of UX design of web pages, mobile apps or figma designs. I used technologies like Vue, Node, Adonis and Typescript.",
    //             image: '/images/uxtweak.png'
    //         },
    //         {
    //             position: "Tour Guide",
    //             company: "CK BALOO",
    //             date: "01/2022 - until now",
    //             description: "I organize extreme expedition tours for slovak adventurers. I create whole tours and then take people on adventure to countries like Iraq, Syria, Mauritania or India.",
    //             image: '/images/baloo.jpeg'
    //         },
    //         {
    //             position: "Big Data Research Assistant",
    //             company: "VÚB Banka",
    //             date: "09/2019 - 12/2019",
    //             description: "This position was gained thanks to good university results. I helped at bid data research with translations of articles and with small programming tasks in python.",
    //             image: '/images/vub.jpg'
    //         },
    //         {
    //             position: "Computer Vision Engineer",
    //             company: "STUBA Green Team",
    //             date: "09/2018 - 02/2019",
    //             description: "Formula Student is tech competition, in which university teams construct formula. I took part in Computer vision team which was trying to construct autonomous formula.",
    //             image: '/images/formula.png'
    //         },
    //         {
    //             position: "Floorball Youth's Coach",
    //             company: "FBK Bratislava",
    //             date: "09/2018 - 02/2019",
    //             description: "When i was forced to end with floorball as player, I established own floorball team for children. It taught me a lot about working with people and mentoring others.",
    //             image: '/images/fbk.jpg'
    //         }
    //     ]
    // }

    static skillsContent: SkillsContent[] = [
        {
            name: 'Jednoduché účtovníctvo',
            level: 4
        },
        {
            name: 'Podvojné účtovníctvo',
            level: 4
        },
        {
            name: 'Fakturácia',
            level: 4
        },
        {
            name: 'Hospodárska korešpondencia',
            level: 4
        },
        {
            name: 'Skladové hospodárstvo',
            level: 4
        },
        {
            name: 'Microsoft Excel',
            level: 4
        },
        {
            name: 'Microsoft Word',
            level: 4
        },
        {
            name: 'Microsoft Outlook',
            level: 4
        },
        {
            name: 'Microsoft Windows',
            level: 4
        },
        {
            name: 'ALFA',
            level: 4
        },
        {
            name: 'OMEGA',
            level: 4
        },
        {
            name: 'POHODA (účtovníctvo)',
            level: 4
        },
        {
            name: 'Pokladňa',
            level: 4
        },
        {
            name: 'Mzdové účtovníctvo',
            level: 2
        },
        {
            name: 'Personalistika',
            level: 2
        }
    ]

}
