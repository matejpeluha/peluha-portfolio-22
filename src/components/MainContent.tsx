import {Texts} from "../texts";
import MainContentHeader from "./MainContentHeader";
import CategoryTimeLine from "./CategoryTimeLine";
import CategoryNavigation from "./CategoryNavigation";
import {useState} from "react";
import SkillsList from "./SkillsList";
import ContactContent from "./ContactContent";

interface MainContentProps {
}

const MainContent = () => {
    const [activeCategory, setActiveCategory] = useState<string>("jobs")

    return (
        <div  className="w-full h-full z-0">
            <MainContentHeader />
            <CategoryNavigation activeCategory={activeCategory} setActiveCategory={setActiveCategory}/>
            {
                Object.keys(Texts.timeLineContent).includes(activeCategory) && <CategoryTimeLine categoryKey={activeCategory} />
            }
            {
                activeCategory === 'skills' && <SkillsList />
            }
            {
                activeCategory === 'contact' && <ContactContent />
            }
        </div>
    )
}

export default MainContent
