import {ContentMenuItem, Texts} from "../texts";
import cx from 'classnames'

interface CategoryNavigationProps {
    activeCategory: string
    setActiveCategory: (contentName: string) => void
}

const CategoryNavigation = ({activeCategory, setActiveCategory}: CategoryNavigationProps) => {
    return (
        <div className="w-full text-white text-lg flex flex-row justify-center ">
            {Texts.menu.map((menuItem: ContentMenuItem, key: number) => {
                const className = cx(
                    "mx-2 sm:mx-5 cursor-pointer select-none transition-all hover:underline underline-offset-8",
                    {
                        "underline": menuItem.key === activeCategory
                    }
                )
                return (
                    <p key={key} onClick={() => setActiveCategory(menuItem.key)} className={className}>
                        {menuItem.title}
                    </p>
                )
            })}
        </div>
    )

}

export default CategoryNavigation
