import {Texts} from "../texts";
import SkillItem from "./SkillItem";
import {FaEnvelope, FaPhone} from "react-icons/fa";

const ContactContent = () => {
    return (
        <div className="flex flex-col items-center mt-16 max-w-[700px] w-full mx-auto pb-10 gap-8">
            <div className="flex flex-col items-center max-w-[500px] w-full mx-auto gap-8 border-2 border-white py-6 rounded-lg">
                <div className="text-white">
                    <a className="flex flex-row items-center gap-4" target='_blank' href={`mailto:${Texts.contact.mail}`}>
                        <FaEnvelope className="text-3xl"/>
                        <span className="text-xl">
                        {Texts.contact.mail}
                    </span>
                    </a>
                </div>
                <div className="text-white">
                    <a className="flex flex-row items-center gap-4" target='_blank' href={`tel:${Texts.contact.phone}`}>
                        <FaPhone className="text-3xl"/>
                        <span className="text-xl">
                        {Texts.contact.phone}
                    </span>
                    </a>
                </div>
            </div>
            <i className="text-white text-2xl mt-10">{Texts.slogan}</i>
        </div>
    )
}

export default ContactContent
