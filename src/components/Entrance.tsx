import React, {useState} from "react";
import {Texts} from "../texts";

interface EntranceProps {
    onEnter: () => void
}

const Entrance = ({onEnter}: EntranceProps) => {
    const [isOpened, setIsOpened] = useState<boolean>(true)

    const handleOnEnter = () => {
        setIsOpened(false)
        onEnter()
    }

    const style = {
        backgroundImage: `url(${process.env.PUBLIC_URL + '/images/title_photo.jpg'})`,
        height: isOpened ? '100%' : 0,
        width: isOpened ? '100%' : 0,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        opacity: isOpened ? 1 : 0,
        transition: 'width 0.8s 0.8s, height 0.8s 0.8s, opacity 0.8s'
    }
      return (
          <div className="absolute inset-0 z-10" style={style}>
              <div className="h-[100%] flex flex-col justify-center gap-10 sm:gap-0">
                  <div className="h-20">
                      <h1 className="text-5xl text-white text-center">{Texts.fullName}</h1>
                  </div>
                  <div >
                      <div onClick={handleOnEnter} className="m-auto text-white text-3xl border-2 border-white p-5 w-44 text-center cursor-pointer select-none drop-shadow transition-all hover:border-white hover:text-red-400 hover:bg-white hover:rounded-lg">
                          {Texts.entranceButton}
                      </div>
                  </div>
              </div>
          </div>
      )
}

export default Entrance
