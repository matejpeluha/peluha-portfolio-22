import {TimelineItem} from "../texts";
import cx from 'classnames'
import { FaSearch } from 'react-icons/fa';

interface CategoryItemProps {
    item: TimelineItem
    side: 'left'|'right'
}

const CategoryItem = ({item, side}: CategoryItemProps) => {
    const contentContainerClassName = cx(
        "flex flex-row w-full flex-wrap text-white gap-3 border-2 border-white rounded-[20px] p-5 bg-zinc-800 drop-shadow-2xl",
        {"flex-row-reverse": side === 'right'}
    )

    const infoClassName = cx(
        "font-bold text-lg",
        {
            "text-right": side === 'right'
        }
    )

    const infoClassName2 = cx(
        "text-lg",
        {
            "text-right": side === 'right'
        }
    )

    const descriptionClassName = cx(
        {
            "text-right": side === 'right'
        }
    )

    const linkClassName = cx(
        {
            "flex flex-row-reverse": side === 'right'
        }
    )

    return (
        <div className="w-full flex flex-col items-center px-5">
            <div className="border-r-2 border-white h-20" />
            <div className={contentContainerClassName}>
                <img className="h-40 w-40 rounded bg-white" src={item.image} alt="item-img"/>
                <div className="flex flex-col justify-center sm:max-w-[425px]">
                    <div className="flex flex-col gap-2">
                        <div className="flex flex-col">
                            <h1 className={infoClassName}>{item.position}</h1>
                            <h1 className={infoClassName2}>{item.company}</h1>
                            <h1 className={infoClassName2}>{item.date}</h1>
                        </div>
                        {
                            item.url && <div className={linkClassName}><a className="w-fit"  target="_blank" href={item.url}><FaSearch/></a></div>
                        }
                        <div className={descriptionClassName}>
                            <p>{item.description}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CategoryItem
