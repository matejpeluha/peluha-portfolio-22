import {SkillsContent} from "../texts";
import {FaStar,FaRegStar} from "react-icons/fa";


interface SkillItemProps {
    skill: SkillsContent
}

const SkillItem = ({skill}: SkillItemProps) => {
    return (
        <div className="flex flex-col items-center">
            <div className="border-r-2 border-white h-16" />
            <div className="flex flex-col justify-center w-[280px] border-2 border-white rounded-[20px] p-5 bg-white drop-shadow-2xl">
                <h1 className="text-center">{skill.name}</h1>
                <div className="flex flex-row justify-center mt-2">
                    {
                        Array.apply(null, Array(skill.level)).map(_ => <FaStar />)
                    }
                    {
                        Array.apply(null, Array(5 - skill.level)).map(_ => <FaRegStar />)
                    }
                </div>
            </div>
        </div>
    )
}

export default SkillItem
