
import CategoryItem from "./CategoryItem";
import {Texts, TimelineItem} from "../texts";
import {FaFilePdf} from "react-icons/fa";

interface CategoryTimeLineProps {
    categoryKey: string
}

const CategoryTimeLine = ({categoryKey}: CategoryTimeLineProps) => {
    let category: TimelineItem[] = []

    Object.entries(Texts.timeLineContent).forEach(([key, value]) => {
        if (key === categoryKey) {
            category = value
        }
    });

    return (
        <div className="flex flex-col items-center mt-5 max-w-[700px] w-full mx-auto pb-10">
            <div className="text-white my-5">
                <a className="flex flex-col justify-center items-center gap-2 hover:text-green-300 transition-all" href={process.env.PUBLIC_URL + Texts.cvDownloadUrl} download>
                    <FaFilePdf className="text-5xl"/>
                    <p className="text-2xl">{Texts.cvDownload}</p>
                </a>
            </div>
            {
                category.map((categoryItem, key) => {
                    const side = key % 2 === 0 ? 'left' : 'right'
                    return <CategoryItem key={key} item={categoryItem} side={side}/>
                })
            }
        </div>
    )
}

export default CategoryTimeLine
