import {Texts} from "../texts";

const MainContentHeader = () => (
    <div className="w-full px-10 pt-20 pb-6 flex flex-col justify-center items-center">
        <img className="rounded-full w-56 h-56 sm:w-72 sm:h-72 drop-shadow-lg" src={process.env.PUBLIC_URL + '/images/profile_2.jpg'} alt="profile"/>
        <h1 className="text-center text-white text-2xl mt-3">{Texts.fullName}</h1>
        <h1 className="text-center text-white text-lg">{Texts.country}, {Texts.birthYear}</h1>
        <div className="w-full mx-5 sm:mx-20 border-b border-white mt-10"/>
    </div>
)

export default MainContentHeader
