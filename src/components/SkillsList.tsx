import {Texts} from "../texts";
import SkillItem from "./SkillItem";
import {FaFilePdf} from "react-icons/fa";

const SkillsList = () => {
    return (
        <div className="flex flex-col items-center mt-5 max-w-[700px] w-full mx-auto pb-10">
            <div className="text-white my-5">
                <a className="flex flex-col justify-center items-center gap-2 hover:text-green-300 transition-all" href={process.env.PUBLIC_URL + Texts.cvDownloadUrl} download>
                    <FaFilePdf className="text-5xl"/>
                    <p className="text-2xl">{Texts.cvDownload}</p>
                </a>
            </div>
            {
                Texts.skillsContent.map((skill, key) => <SkillItem key={key} skill={skill}/>)
            }
        </div>
    )
}

export default SkillsList
